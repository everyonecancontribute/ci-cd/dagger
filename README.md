# Dagger

An example project for running an application with dagger.io to run [dagger.io](https://dagger.io).

* It builds an ToDo App and can be used to publish it at netlify

The result [can be seen here](https://ecc-todoapp.netlify.app/)

* Last tested version: `dagger v0.2.5 (7d2f279c) darwin/amd64`

## Requirements

* Docker Engine (Docker for Desktop, Docker)
  * with BuildkitD
* [Dagger](https://docs.dagger.io/1200/local-dev)

## Getting Started

```bash
git clone https://gitlab.com/everyonecancontribute/ci-cd/dagger
cd dagger
```

### Initialize your Dagger Project

```bash
dagger project update
```

### Build the app

```bash
dagger do build
```

```bash
dagger do build
[✔] actions.build.run.script                                                                                                    0.0s
[✔] actions.deps                                                                                                                0.8s
[✔] actions.test.script                                                                                                         0.0s
[✔] client.filesystem."./".read                                                                                                 0.1s
[✔] actions.test                                                                                                                0.0s
[✔] actions.build.run                                                                                                           0.0s
[✔] actions.build.contents                                                                                                      0.0s
[✔] client.filesystem."./_build".write                                                                                          0.1s
```

### Deploy the App

```bash
# This needs to be the app name for netlify
export APP_NAME=ecc-todapp
export NETLIFY_TEAM=<YOUR_NETLIFY_TEAM_NAME>
```

You also need to set a [Personal Access Token for deploying netlify](https://app.netlify.com/user/applications/personal)

```bash
export NETLIFY_TOKEN=<YOUR_NETLIFY_TOKEN>
```

Finally you can execute the deploy action

```console
dagger do deploy
[✔] actions.deploy.container.script                                                                                                                                0.2s
[✔] actions.build.run.script                                                                                                                                       0.0s
[✔] actions.test.script                                                                                                                                            0.0s
[✔] actions.deploy                                                                                                                                                74.8s
[✔] client.env                                                                                                                                                     0.0s
[✔] actions.deps                                                                                                                                                   6.2s
[✔] client.filesystem."./".read                                                                                                                                    0.3s
[✔] actions.test                                                                                                                                                   3.3s
[✔] actions.build.run                                                                                                                                             14.1s
[✔] actions.build.contents                                                                                                                                         0.0s
[✔] client.filesystem."./_build".write                                                                                                                             0.2s
[✔] actions.deploy.container                                                                                                                                      29.6s
[✔] actions.deploy.container.export                                                                                                                                0.0s
```

## Troubleshoot

I can't deploy my app

```bash
dagger do deploy
[✗] actions.deploy.container.script                                                                                             0.0s
[✗] client.env                                                                                                                  0.0s
[✗] actions.deps                                                                                                                0.0s
[✗] actions.build.run.script                                                                                                    0.0s
[✗] actions.deploy                                                                                                              0.0s
[✗] actions.test.script                                                                                                         0.0s
[✗] client.filesystem."./".read                                                                                                 0.0s
10:58AM FTL failed to execute plan: task failed: client.env: environment variable "APP_NAME" not set
```

## How to contribute?

Contributions are always welcome. Don't be shy!
